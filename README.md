# MovieCharacterAPI

MovieCharacterAPI is an example API that allows the user to see how an API could be set up with .net 5 and EF.

The example use DTOs and Services.

## Prerequisites

Before you begin, ensure you have met the following requirements:

* You have visual studio or visual studio code installed, with .net 5 and ASP.net support (You might be able to use other IDEs)

## Installing MovieCharacterAPI

To install MovieCharacterAPI, follow these steps:

Open git bash

Paste:
`git clone git@gitlab.com:MortenTobiasNielsen/moviecharacterapi.git`

## Using MovieCharacterAPI

To use MovieCharacterAPI, follow these steps:

Open the solution in your IDE and run the project. 
A websie will open up (standard swagger documentation) and you will be able to see the end points of the application and trying them.

## Contributing to MovieCharacterAPI
To contribute to MovieCharacterAPI, follow these steps:

1. Fork this repository.
2. Create a branch: `git checkout -b <branch_name>`.
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Push to the original branch: `git push origin https://gitlab.com/MortenTobiasNielsen/moviecharacterapi/`
5. Create the pull request.

Alternatively see the GitHub documentation on [creating a pull request](https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/creating-a-pull-request).

## Contributors

Thanks to the following people who have contributed to this project:

You name will go here if you contribute. :)

## Contact

Please make an issue with your request.

## License

This project uses the following license: [MIT](https://choosealicense.com/licenses/mit/).
