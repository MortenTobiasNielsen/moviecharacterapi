﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MovieCharacterAPI.Migrations
{
    public partial class changeReleaseYearDataType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<short>(
                name: "ReleaseYear",
                table: "Movies",
                type: "smallint",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ReleaseYear", "Trailer" },
                values: new object[] { (short)2008, null });

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ReleaseYear", "Trailer" },
                values: new object[] { (short)2010, null });

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "ReleaseYear", "Trailer" },
                values: new object[] { (short)2001, null });

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "ReleaseYear", "Trailer" },
                values: new object[] { (short)2000, null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "ReleaseYear",
                table: "Movies",
                type: "int",
                nullable: false,
                oldClrType: typeof(short),
                oldType: "smallint");

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ReleaseYear", "Trailer" },
                values: new object[] { 2008, "" });

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ReleaseYear", "Trailer" },
                values: new object[] { 2010, "" });

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "ReleaseYear", "Trailer" },
                values: new object[] { 2001, "" });

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "ReleaseYear", "Trailer" },
                values: new object[] { 2000, "" });
        }
    }
}
