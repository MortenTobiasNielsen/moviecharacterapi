﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.Models {
    public class Character {
        // Private Key
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]

        // Fields
        public string FullName { get; set; }
        [MaxLength(50)]
        public string Alias { get; set; }
        [Required]
        [MaxLength(6)]
        public string Gender { get; set; }
        [MaxLength(200)]
        public string Picture { get; set; }

        // Relationships
        public ICollection<Movie> Movies { get; set; }
    }
}
