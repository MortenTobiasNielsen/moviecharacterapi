﻿namespace MovieCharacterAPI.Models.DTO.Movie {
    public class MovieReadDTO {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public short ReleaseYear { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
        public int FranchiseId { get; set; }
    }
}
