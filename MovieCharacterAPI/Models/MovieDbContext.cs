﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace MovieCharacterAPI.Models {
    public class MovieDbContext : DbContext {
        // Tables
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        public MovieDbContext([NotNull] DbContextOptions options) : base(options) {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            // Seed Movie Data
            modelBuilder.Entity<Movie>().HasData(new Movie { 
                Id = 1, 
                Title = "Iron Man", 
                Genre = "Aciton, Adventure, Sci-Fi", 
                ReleaseYear=2008, 
                Director= "Jon Favreau", 
                Picture= "https://sm.ign.com/ign_nordic/news/m/marvels-ir/marvels-iron-man-vr-new-combat-world-details-revealed_y3fn.jpg", 
                FranchiseId=1 
            });
            modelBuilder.Entity<Movie>().HasData(new Movie { 
                Id = 2, 
                Title = "Iron Man 2",
                Genre = "Aciton, Adventure, Sci-Fi",
                ReleaseYear = 2010,
                Director = "Jon Favreau",
                Picture = "https://sm.ign.com/ign_nordic/news/m/marvels-ir/marvels-iron-man-vr-new-combat-world-details-revealed_y3fn.jpg",
                FranchiseId = 1
            });
            modelBuilder.Entity<Movie>().HasData(new Movie {
                Id = 3,
                Title = "The Lord of the Rings: The Fellowship of the Ring",
                Genre = "Aciton, Adventure, Drama",
                ReleaseYear = 2001,
                Director = "Peter Jackson",
                Picture = "https://m.media-amazon.com/images/M/MV5BN2EyZjM3NzUtNWUzMi00MTgxLWI0NTctMzY4M2VlOTdjZWRiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_.jpg",
                FranchiseId = 2
            });
            modelBuilder.Entity<Movie>().HasData(new Movie {
                Id = 4,
                Title = "X-Men",
                Genre = "Aciton, Adventure, Sci-Fi",
                ReleaseYear = 2000,
                Director = "Bryan Singer",
                Picture = "https://lumiere-a.akamaihd.net/v1/images/x-men_1152x1704_29b7bfce.jpeg",
                FranchiseId = 3
            });

            // Seed Franchise Data
            modelBuilder.Entity<Franchise>().HasData(new Franchise() {
                Id = 1,
                Name = "Avengers",
                Description = "The Marvel Cinematic Universe (MCU) films are a series of American superhero films produced by Marvel Studios based on characters that appear in publications by Marvel Comics."
            });
            modelBuilder.Entity<Franchise>().HasData(new Franchise() {
                Id = 2,
                Name = "The Lord of the Rings",
                Description = "The Lord of the Rings is a series of three epic fantasy adventure films directed by Peter Jackson, based on the novel written by J. R. R. Tolkien."
            });
            modelBuilder.Entity<Franchise>().HasData(new Franchise() {
                Id = 3,
                Name = "X-Men",
                Description = "X-Men is an American superhero film series based on the fictional superhero team of the same name, who originally appeared in a series of comic books created by Stan Lee and Jack Kirby and published by Marvel Comics."
            });

            // Seed Character Data
            modelBuilder.Entity<Character>().HasData(new Character() {
                Id = 1,
                FullName = "Iron Man",
                Alias = "Tony Stark",
                Gender = "Male"
            });
            modelBuilder.Entity<Character>().HasData(new Character() {
                Id = 2,
                FullName = "Peppers Potts",
                Gender = "Female"
            });
            modelBuilder.Entity<Character>().HasData(new Character() {
                Id = 3,
                FullName = "Gandalf",
                Alias = "Olorin",
                Gender = "Male"
            });
            modelBuilder.Entity<Character>().HasData(new Character() {
                Id = 4,
                FullName = "Aragorn",
                Alias = "Strider",
                Gender = "Male"
            });
            modelBuilder.Entity<Character>().HasData(new Character() {
                Id = 5,
                FullName = "Wolverine",
                Alias = "Logan",
                Gender = "Male"
            });
            modelBuilder.Entity<Character>().HasData(new Character() {
                Id = 6,
                FullName = "Mystique",
                Alias = "Raven Darkhölme",
                Gender = "Female"
            });
            modelBuilder.Entity<Character>().HasData(new Character() {
                Id = 7,
                FullName = "Storm",
                Alias = "Ororo Munroe",
                Gender = "Female"
            });

            // Seed many to many Movie-Character linking table
            modelBuilder.Entity<Movie>()
                .HasMany(p => p.Characters)
                .WithMany(m => m.Movies)
                .UsingEntity<Dictionary<string, object>>(
                    "CharacterMovie",
                    r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    je => {
                        je.HasKey("MovieId", "CharacterId");
                        je.HasData(
                            new { MovieId = 1, CharacterId = 1 },
                            new { MovieId = 2, CharacterId = 1 },
                            new { MovieId = 1, CharacterId = 2 },
                            new { MovieId = 2, CharacterId = 2 },
                            new { MovieId = 3, CharacterId = 3 },
                            new { MovieId = 3, CharacterId = 4 },
                            new { MovieId = 4, CharacterId = 5 },
                            new { MovieId = 4, CharacterId = 6 },
                            new { MovieId = 4, CharacterId = 7 }
                        );
                    });
        }
    }
}
