﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.DTO.Character;
using MovieCharacterAPI.Services;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Controllers {
    [Route("api/v1/characters")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase {
        private readonly IMapper _mapper;
        private readonly ICharacterService _characterService;

        public CharactersController(ICharacterService characterService, IMapper mapper) {
            _mapper = mapper;
            _characterService = characterService;
        }

        /// <summary>
        /// Gets all the characters
        /// </summary>
        /// <returns>A list of all the characters in the database</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters() {
            return _mapper.Map<List<CharacterReadDTO>>(await _characterService.GetAllCharactersAsync());
        }

        /// <summary>
        /// Gets a specific character by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns the specified character, if it exists</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id) {
            Character character = await _characterService.GetSpecificCharacterAsync(id);

            if (character == null) {
                return NotFound();
            }

            return _mapper.Map<CharacterReadDTO>(character);
        }

        /// <summary>
        /// Updates a specific character based on an id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="character"></param>
        /// <returns>An Http response code</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, [FromBody] CharacterEditDTO character) {
            if (id != character.Id) {
                return BadRequest();
            }

            if (!_characterService.CharacterExists(id)) {
                return NotFound();
            }

            Character domainCharacter = _mapper.Map<Character>(character);
            await _characterService.UpdateCharacterAsync(domainCharacter);

            return NoContent();
        }

        /// <summary>
        /// Adds a new character
        /// </summary>
        /// <param name="character"></param>
        /// <returns>An Http response code and the new entry in the database</returns>
        [HttpPost]
        public async Task<ActionResult<CharacterReadDTO>> PostCharacter([FromBody] CharacterCreateDTO character) {
            Character domainCharacter = _mapper.Map<Character>(character);
            await _characterService.AddCharacterAsync(domainCharacter);

            return CreatedAtAction("GetCharacter",
                new { id = domainCharacter.Id },
                _mapper.Map<CharacterReadDTO>(domainCharacter)
                );
        }

        /// <summary>
        /// Deletes a specified character by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>An Http response code</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id) {
            if (await _characterService.DeleteCharacterAsync(id)) {
                return NoContent();
            }

            return NotFound();
        }
    }
}
