﻿using AutoMapper;
using FranchiseCharacterAPI.Services;
using Microsoft.AspNetCore.Mvc;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.DTO.Character;
using MovieCharacterAPI.Models.DTO.Franchise;
using MovieCharacterAPI.Models.DTO.Movie;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Controllers {
    [Route("api/v1/franchises")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase {
        private readonly IMapper _mapper;
        private readonly IFranchiseService _franchiseService;

        public FranchisesController(IFranchiseService franchiseService, IMapper mapper) {
            _mapper = mapper;
            _franchiseService = franchiseService;
        }

        /// <summary>
        /// Gets all the franchises
        /// </summary>
        /// <returns>A list of all the franchises in the database</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises() {
            return _mapper.Map<List<FranchiseReadDTO>>(await _franchiseService.GetAllFranchisesAsync());
        }

        /// <summary>
        /// Gets a specific franchise by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns the specified franchise, if it exists</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id) {
            Franchise franchise = await _franchiseService.GetSpecificFranchiseAsync(id);

            if (franchise == null) {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

        /// <summary>
        /// Gets all movies in a franchise by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A list of all the movies in a franchise available in the database</returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetFranchiseMovies(int id) {
            return _mapper.Map<List<MovieReadDTO>>(await _franchiseService.GetFranchiseMoviesAsync(id));
        }

        /// <summary>
        /// Gets all characters in a franchise by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A list of all the characters in a franchise available in the database</returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetFranchiseCharacters(int id) {
            IEnumerable<Character> characterCollection = await _franchiseService.GetFranchiseCharactersAsync(id);

            return _mapper.Map<List<CharacterReadDTO>>(characterCollection.ToList());
        }

        /// <summary>
        /// Updates a specific franchise based on an id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchise"></param>
        /// <returns>An Http response code</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO franchise) {
            if (id != franchise.Id) {
                return BadRequest();
            }

            if (!_franchiseService.FranchiseExists(id)) {
                return NotFound();
            }


            Franchise domainFranchie = _mapper.Map<Franchise>(franchise);
            await _franchiseService.UpdateFranchiseAsync(domainFranchie);

            return NoContent();
        }

        /// <summary>
        /// Adds movies to a franchise based on movie ids
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movieIds"></param>
        /// <returns>An Http response code</returns>
        [HttpPatch("{id}/movies")]
        public async Task<IActionResult> UpdateFranchiseMovies(int id, int[] movieIds) {
            if (!_franchiseService.FranchiseExists(id)) {
                return BadRequest();
            }

            if (await _franchiseService.UpdateFranchiseMoviesAsync(id, movieIds)) {
                return NoContent();
            }

            return BadRequest("Movie doesn't exist!");
        }

        /// <summary>
        /// Adds a new franchise
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns>An Http response code and the new entry in the database</returns>
        [HttpPost]
        public async Task<ActionResult<FranchiseReadDTO>> PostFranchise(FranchiseCreateDTO franchise) {
            Franchise domainFranchise = _mapper.Map<Franchise>(franchise);
            await _franchiseService.AddFranchiseAsync(domainFranchise);

            return CreatedAtAction("GetFranchise",
                new { id = domainFranchise.Id },
                _mapper.Map<FranchiseReadDTO>(domainFranchise));
        }

        /// <summary>
        /// Deletes a specified franchise by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>An Http response code</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id) {
            if (await _franchiseService.DeleteFranchiseAsync(id)) {
                return NoContent();
            }

            return NotFound();
        }
    }
}
