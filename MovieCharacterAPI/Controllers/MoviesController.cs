﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.DTO.Character;
using MovieCharacterAPI.Models.DTO.Movie;
using MovieCharacterAPI.Services;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Controllers {
    [Route("api/v1/movies")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MoviesController : ControllerBase {
        private readonly IMapper _mapper;
        private readonly IMovieService _movieService;

        public MoviesController(IMovieService movieService, IMapper mapper) {
            _mapper = mapper;
            _movieService = movieService;
        }

        /// <summary>
        /// Gets all the movies
        /// </summary>
        /// <returns>A list of all the movies in the database</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies() {
            return _mapper.Map<List<MovieReadDTO>>(await _movieService.GetAllMoviesAsync());
        }

        /// <summary>
        /// Gets a specific movie by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns the specified movie, if it exists</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id) {
            Movie movie = await _movieService.GetSpecificMovieAsync(id);

            if (movie == null) {
                return NotFound();
            }

            return _mapper.Map<MovieReadDTO>(movie);
        }

        /// <summary>
        /// Gets all the characters in a movie by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A list of all the characters in a movie available in the database</returns>
        [HttpGet("{id}/Characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetMoviesCharacters(int id) {
            return _mapper.Map<List<CharacterReadDTO>>(await _movieService.GetMovieCharactersAsync(id));
        }

        /// <summary>
        /// Updates a specific movie based on an id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movie"></param>
        /// <returns>An Http response code</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieEditDTO movie) {
            if (id != movie.Id) {
                return BadRequest();
            }

            if (!_movieService.MovieExists(id)) {
                return NotFound();
            }

            Movie domainMovie = _mapper.Map<Movie>(movie);
            await _movieService.UpdateMovieAsync(domainMovie);

            return NoContent();
        }

        /// <summary>
        /// Updates the characters of a movie to be the specified character ids
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characterIds"></param>
        /// <returns>An Http response code</returns>
        [HttpPut("{id}/characters")]
        public async Task<IActionResult> UpdateMovieCharacters(int id, int[] characterIds) {
            if (!_movieService.MovieExists(id)) {
                return NotFound();
            }

            if(await _movieService.UpdateMovieCharactersAsync(id, characterIds)) {
                return NoContent();
            }

            return BadRequest();
        }

        /// <summary>
        /// Adds a new movie
        /// </summary>
        /// <param name="movie"></param>
        /// <returns>An Http response code and the new entry in the database</returns>
        [HttpPost]
        public async Task<ActionResult<MovieReadDTO>> PostMovie(MovieCreateDTO movie) {
            Movie domainMovie = _mapper.Map<Movie>(movie);
            await _movieService.AddMovieAsync(domainMovie);

            return CreatedAtAction("GetMovie",
                new { id = domainMovie.Id }, 
                _mapper.Map<MovieReadDTO>(domainMovie));
        }

        /// <summary>
        /// Deletes a specified movie by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>An Http response code</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id) {
            if (await _movieService.DeleteMovieAsync(id)) {
                return NoContent();
            }

            return NotFound();
        }
    }
}
