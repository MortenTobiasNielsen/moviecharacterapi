﻿using MovieCharacterAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Services {
    public interface ICharacterService {
        /// <summary>
        /// Gets all the characters
        /// </summary>
        /// <returns>A list of all the characters in the database</returns>
        public Task<IEnumerable<Character>> GetAllCharactersAsync();

        /// <summary>
        /// Gets a specific character by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns the specified character, if it exists</returns>
        public Task<Character> GetSpecificCharacterAsync(int id);

        /// <summary>
        /// Adds a new character
        /// </summary>
        /// <param name="character"></param>
        /// <returns>An Http response code and the new entry in the database</returns>
        public Task AddCharacterAsync(Character character);

        /// <summary>
        /// Updates a specific character
        /// </summary>
        /// <param name="character"></param>
        /// <returns>An Http response code</returns>
        public Task UpdateCharacterAsync(Character character);

        /// <summary>
        /// Deletes a specified character by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>An Http response code</returns>
        public Task<bool> DeleteCharacterAsync(int id);

        /// <summary>
        /// Checks if the character exists in the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>True if the character exists. False otherwise</returns>
        public bool CharacterExists(int id);
    }
}
