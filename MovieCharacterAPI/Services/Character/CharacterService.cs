﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Services {
    public class CharacterService : ICharacterService {
        private readonly MovieDbContext _context;

        public CharacterService(MovieDbContext context) {
            _context = context;
        }

        public async Task AddCharacterAsync(Character character) {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();
        }

        public bool CharacterExists(int id) {
            return _context.Characters.Any(e => e.Id == id); ;
        }

        public async Task<bool> DeleteCharacterAsync(int id) {
            Character character = await _context.Characters.FindAsync(id);
            if (character == null) {
                return false;
            }

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<IEnumerable<Character>> GetAllCharactersAsync() {
            return await _context.Characters.ToListAsync();
        }

        public async Task<Character> GetSpecificCharacterAsync(int id) {
            return await _context.Characters.FindAsync(id);
        }

        public async Task UpdateCharacterAsync(Character character) {
            _context.Entry(character).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
