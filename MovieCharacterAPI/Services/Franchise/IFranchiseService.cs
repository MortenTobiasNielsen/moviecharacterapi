﻿using MovieCharacterAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FranchiseCharacterAPI.Services {
    public interface IFranchiseService {
        /// <summary>
        /// Gets all the franchises
        /// </summary>
        /// <returns>A list of all the franchises in the database</returns>
        public Task<IEnumerable<Franchise>> GetAllFranchisesAsync();

        /// <summary>
        /// Gets a specific franchise by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns the specified franchise, if it exists</returns>
        public Task<Franchise> GetSpecificFranchiseAsync(int id);

        /// <summary>
        /// Gets all the characters in a franchise by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A list of all the characters in a franchise available in the database</returns>
        public Task<IEnumerable<Character>> GetFranchiseCharactersAsync(int id);

        /// <summary>
        /// Gets all the movies in a franchise by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A list of all the movies in a franchise available in the database</returns>
        public Task<IEnumerable<Movie>> GetFranchiseMoviesAsync(int id);

        /// <summary>
        /// Adds a new franchise
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns>An Http response code and the new entry in the database</returns>
        public Task AddFranchiseAsync(Franchise franchise);

        /// <summary>
        /// Updates a specific franchise
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns>An Http response code</returns>
        public Task UpdateFranchiseAsync(Franchise franchise);

        /// <summary>
        /// Updates the movies of a franchise to be the specified movie ids
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movieIds"></param>
        /// <returns>An Http response code</returns>
        public Task<bool> UpdateFranchiseMoviesAsync(int id, int[] movieIds);

        /// <summary>
        /// Deletes a specified franchise by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>An Http response code</returns>
        public Task<bool> DeleteFranchiseAsync(int id);

        /// <summary>
        /// Checks if the moive exists in the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>True if the franchise exists. False otherwise</returns>
        public bool FranchiseExists(int id);
    }
}
