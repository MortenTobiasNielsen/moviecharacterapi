﻿using FranchiseCharacterAPI.Services;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Services {
    public class FranchiseService : IFranchiseService {
        private readonly MovieDbContext _context;

        public FranchiseService(MovieDbContext context) {
            _context = context;
        }

        /// <summary>
        /// Adds a new franchise
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns>An Http response code and the new entry in the database</returns>
        public async Task AddFranchiseAsync(Franchise franchise) {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Deletes a specified franchise by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>An Http response code</returns>
        public async Task<bool> DeleteFranchiseAsync(int id) {
            Franchise franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null) {
                return false;
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// Checks if the franchise exists in the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>True if the franchise exists. False otherwise</returns>
        public bool FranchiseExists(int id) {
            return _context.Franchises.Any(e => e.Id == id);
        }

        /// <summary>
        /// Gets all the franchises
        /// </summary>
        /// <returns>A list of all the franchises in the database</returns>
        public async Task<IEnumerable<Franchise>> GetAllFranchisesAsync() {
            return await _context.Franchises.ToListAsync();
        }

        /// <summary>
        /// Gets all characters in a franchise by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A list of all the characters in a franchise available in the database</returns>
        public async Task<IEnumerable<Character>> GetFranchiseCharactersAsync(int id) {
            return await _context.Movies.Where(m => m.FranchiseId == id)
                                        .SelectMany(m => m.Characters)
                                        .Distinct()
                                        .ToListAsync();
        }

        /// <summary>
        /// Gets all movies in a franchise by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A list of all the movies in a franchise available in the database</returns>
        public async Task<IEnumerable<Movie>> GetFranchiseMoviesAsync(int id) {
            return await _context.Movies.Where(m => m.FranchiseId == id).ToListAsync();
        }

        /// <summary>
        /// Gets a specific franchise by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns the specified franchise, if it exists</returns>
        public async Task<Franchise> GetSpecificFranchiseAsync(int id) {
            return await _context.Franchises.FindAsync(id);
        }

        /// <summary>
        /// Updates a specific franchise based on an id
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns>An Http response code</returns>
        public async Task UpdateFranchiseAsync(Franchise franchise) {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task<bool> UpdateFranchiseMoviesAsync(int id, int[] movieIds) {
            foreach (int movieId in movieIds) {
                Movie movieToUpdate = await _context.Movies.FindAsync(movieId);

                if (movieToUpdate == null) {
                    return false;
                }

                movieToUpdate.FranchiseId = id;
            }

            await _context.SaveChangesAsync();

            return true;
        }
    }
}
