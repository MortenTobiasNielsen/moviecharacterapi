﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Services {
    public class MovieService : IMovieService {
        private readonly MovieDbContext _context;

        public MovieService(MovieDbContext context) {
            _context = context;
        }

        public async Task AddMovieAsync(Movie movie) {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
        }

        public async Task<bool> DeleteMovieAsync(int id) {
            Movie movie = await _context.Movies.FindAsync(id);
            if (movie == null) {
                return false;
            }

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<IEnumerable<Movie>> GetAllMoviesAsync() {
            return await _context.Movies.ToListAsync();
        }

        public async Task<IEnumerable<Character>> GetMovieCharactersAsync(int id) {
            return await _context.Movies.Where(m => m.Id == id)
                                        .SelectMany(m => m.Characters)
                                        .ToListAsync();
        }

        public async Task<Movie> GetSpecificMovieAsync(int id) {
            return await _context.Movies.FindAsync(id);
        }

        public bool MovieExists(int id) {
            return _context.Movies.Any(e => e.Id == id);
        }

        public async Task UpdateMovieAsync(Movie movie) {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task<bool> UpdateMovieCharactersAsync(int id, int[] characterIds) {
            Movie movieToUpdateCharacters = await _context.Movies
                .Include(m => m.Characters)
                .Where(m => m.Id == id)
                .FirstAsync();

            List<Character> characters = await _context.Characters.Where(c => characterIds.Contains(c.Id))
                                                                  .ToListAsync();

            if (characterIds.Length > characters.Count) {
                return false;
            }

            movieToUpdateCharacters.Characters = characters;
            await _context.SaveChangesAsync();

            return true;
        }
    }
}
