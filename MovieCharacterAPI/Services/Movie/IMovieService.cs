﻿using MovieCharacterAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Services {
    public interface IMovieService {
        /// <summary>
        /// Gets all the movies
        /// </summary>
        /// <returns>A list of all the movies in the database</returns>
        public Task<IEnumerable<Movie>> GetAllMoviesAsync();

        /// <summary>
        /// Gets a specific movie by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns the specified movie, if it exists</returns>
        public Task<Movie> GetSpecificMovieAsync(int id);

        /// <summary>
        /// Gets all the characters in a movie by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A list of all the characters in a movie available in the database</returns>
        public Task<IEnumerable<Character>> GetMovieCharactersAsync(int id);

            /// <summary>
            /// Adds a new movie
            /// </summary>
            /// <param name="movie"></param>
            /// <returns>An Http response code and the new entry in the database</returns>
            public Task AddMovieAsync(Movie movie);

        /// <summary>
        /// Updates a specific movie
        /// </summary>
        /// <param name="movie"></param>
        /// <returns>An Http response code</returns>
        public Task UpdateMovieAsync(Movie movie);

        /// <summary>
        /// Updates the characters of a movie to be the specified character ids
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characterIds"></param>
        /// <returns>An Http response code</returns>
        public Task<bool> UpdateMovieCharactersAsync(int id, int[] characterIds);

        /// <summary>
        /// Deletes a specified movie by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>An Http response code</returns>
        public Task<bool> DeleteMovieAsync(int id);

        /// <summary>
        /// Checks if the moive exists in the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>True if the movie exists. False otherwise</returns>
        public bool MovieExists(int id);
    }
}
