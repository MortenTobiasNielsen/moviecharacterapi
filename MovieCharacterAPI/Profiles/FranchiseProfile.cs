﻿using AutoMapper;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.DTO.Franchise;

namespace MovieCharacterAPI.Profiles {
    public class FranchiseProfile : Profile {
        public FranchiseProfile() {
            CreateMap<Franchise, FranchiseReadDTO>().ReverseMap();
            CreateMap<Franchise, FranchiseEditDTO>().ReverseMap();
            CreateMap<Franchise, FranchiseCreateDTO>().ReverseMap();
        }
    }
}
