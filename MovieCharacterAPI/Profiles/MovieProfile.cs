﻿using AutoMapper;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.DTO.Movie;

namespace MovieCharacterAPI.Profiles {
    public class MovieProfile : Profile {
        public MovieProfile() {
            CreateMap<Movie, MovieReadDTO>().ReverseMap();
            CreateMap<Movie, MovieEditDTO>().ReverseMap();
            CreateMap<Movie, MovieCreateDTO>().ReverseMap();
        }
    }
}
