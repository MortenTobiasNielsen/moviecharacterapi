﻿using AutoMapper;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.DTO.Character;

namespace MovieCharacterAPI.Profiles {
    public class CharacterProfile : Profile {
        
        public CharacterProfile() {
            CreateMap<Character, CharacterReadDTO>().ReverseMap();
            CreateMap<Character, CharacterEditDTO>().ReverseMap();
            CreateMap<Character, CharacterCreateDTO>().ReverseMap();
        }
    }
}
